from pyvis.network import Network


class Plot:

    def __init__(self, *args):
        if len(args) == 1:
            self.V = args[0].V
            self.E = tuple(args[0].E)
            self.name = "graph"
        elif len(args) == 2:
            self.V = args[0]
            self.E = args[1]
            self.name = "graph"
        elif len(args) == 3:
            self.V = args[0]
            self.E = args[1]
            self.name = args[2]

        plot_graph(self)
        

def plot_graph(self):

    nt = Network('800px', '1900px')
    nt.add_nodes(self.V)
    nt.add_edges(self.E)
    nt.force_atlas_2based()

    nt.set_options("""
    var options = {
  "nodes": {
    "color": {
      "border": "rgba(0,0,0,1)",
      "background": "rgba(255,254,251,1)",
      "highlight": {
        "border": "rgba(0,0,0,1)",
        "background": "rgba(0,0,0,1)"
      },
    "font": {
      "size": 32
      }
    },
    "size": 30
  },
  "edges": {
    "color": {
      "color": "rgba(94,149,204,1)",
      "highlight": "rgba(0,0,0,1)",
      "hover": "rgba(101,167,196,1)",
      "inherit": false
    },
    "physics": false,
    "selectionWidth": 3.5,
    "smooth": false
  },
  "physics": {
    "forceAtlas2Based": {
      "gravitationalConstant": -169,
      "centralGravity": 0.005,
      "springLength": 100
    },
    "minVelocity": 0.75,
    "solver": "forceAtlas2Based"
  }
}
    """)

    nt.show(self.name + '.html')
    

if __name__ == '__main__':
    plot_graph()
