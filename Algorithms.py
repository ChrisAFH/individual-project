from GraphPlot import Plot
from Graph import Graph
import time
import sys
from collections import deque

# increase the recursive limit to 100,000 for DFS
sys.setrecursionlimit(10 ** 5)


def breadth_first_search(G, s):
    Q = deque()
    visited = []
    p = []
    o = []

    # Initialises the empty arrays
    for v in G.V:
        visited.append(False)
        p.append(None)

    # starts from 0 or given source
    if s is None:
        Q.append(G.V[0])
        visited[0] = True
    else:
        Q.append(s)
        visited[s] = True

    while len(Q) > 0:
        # Removes vertex from front of the queue and makes the active vertex
        v = Q.popleft()
        o.append(v)
        # Adds all neighbours in list to queue
        if G.is_matrix == 0:
            for i in G.N[v]:
                if not visited[i]:
                    Q.append(i)
                    p[i] = v
                    visited[i] = True
        else:
            # Adds all neighbours in matrix to queue
            for i in range(len(G.V)):
                if G.N[v][i] == 1 and not visited[i]:
                    Q.append(i)
                    p[i] = v
                    visited[i] = True
    # Returns the parent array and the ordering
    return p, o


def preparation(G, s):
    # Creates empty parent array, ordering array and discovered array
    parent = []
    ordering = []
    discovered = []

    # Loops through all vertices and initialises the arrays
    for v in G.V:
        discovered.append(False)
        parent.append(None)

    # Calls main algorithm with starting variables
    depth_first_search(G, s, discovered, parent, ordering)

    # Returns the parent array and ordering to the user
    return parent, ordering


def depth_first_search(G, s, discovered, parent, ordering):
    # Sets the given source as discovered and adds to ordering
    discovered[s] = True
    ordering.append(s)

    # Goes through all neighbours in list and recursively calls
    # itself
    if G.is_matrix == 0:
        for i in G.N[s]:
            if not discovered[i]:
                parent[i] = s
                depth_first_search(G, i, discovered, parent, ordering)
    # Goes through all neighbours in list and recursively calls
    # itself
    else:
        for i in range(len(G.V)):
            if G.N[s][i] == 1 and not discovered[i]:
                parent[i] = s
                depth_first_search(G, i, discovered, parent, ordering)


def lex_breadth_first_search(G, s):
    # Initialises empty labels
    labels = [[] for v in G.V]
    ordering = []

    # Populates source vertex's label
    labels[s] = [len(G.V)]

    for i in range(len(G.V)):
        # Gets the vertex with the largest label and stores it in v
        v = len(G.V) - labels[::-1].index(max(labels)) - 1
        ordering.append(v)

        if G.is_matrix == 0:
            for n in G.N[v]:
                if n not in ordering:
                    labels[n].append(len(G.V) - (n + 1))

        else:
            # Adds all neighbours in matrix to queue
            for n in range(len(G.V)):
                if n not in ordering and G.N[v][n] == 1:
                    labels[n].append(len(G.V) - (n + 1))

        labels[v] = []

    return ordering


def set_partition_lex_bfs(G, s):
    data = []
    # Initialises ordering
    alpha = [-1 for i in range(len(G.V))]
    ordering = []

    # Adds header sets 
    data.append([0, 1, -1, -1])
    data.append([0, -1, -1, 0])
    c = 2

    # Populates first set
    for v in G.V:
        data.append([1, 0, 0, 0])
        data[c - 1][2] = c
        data[c][1] = v
        data[c][3] = c - 1
        c += 1
    # Adds end of list signifier
    data[c - 1][2] = -1

    # Loops through all vertices
    for i in range(len(G.V)):
        # Skips empty sets
        while data[data[0][1]][2] == -1:
            data[0][1] = data[data[0][1]][1]
            data[data[0][1]][3] = 0

        # Selects next vertex to number
        p = data[data[0][1]][2]

        # Removes pointers in linked list
        data[data[0][1]][2] = data[p][2]
        if data[p][2] != -1:
            data[data[p][2]][3] = data[p][3]

        # Numbers vertex and then empties set entry
        v = data[p][1]
        alpha[v] = i
        ordering.append(v)
        data[p] = 0

        # Loops through neighbours
        if G.is_matrix == 0:
            for n in G.N[v]:
                # Checks if numbered
                if alpha[n] == -1:

                    # gets index for cell of neighbour
                    n = n + 2

                    # Removes neighbour from list
                    data[data[n][3]][2] = data[n][2]
                    if data[n][2] != -1:
                        data[data[n][2]][3] = data[n][3]

                    # Gets header of previous set
                    h = data[data[n][0]][3]

                    # If the previous set is old
                    # makes a new set to store neighbour
                    if data[h][0] <= i:
                        head = data[h][1]
                        data[h][1] = c
                        data[head][3] = c
                        back = h
                        flag = i + 1
                        data.append([flag, head, -1, back])
                        h = c
                        c += 1

                    # Adds neighbour to new set
                    data[n][2] = data[h][2]
                    if data[h][2] != -1:
                        data[data[h][2]][3] = n

                    data[n][0] = h
                    data[n][3] = h
                    data[h][2] = n
        else:
            for n in range(len(G.V)):
                # Checks if numbered
                if G.N[v][n] == 1 and alpha[n] == -1:

                    # gets index for cell of neighbour
                    n = n + 2

                    # Removes neighbour from list
                    data[data[n][3]][2] = data[n][2]
                    if data[n][2] != -1:
                        data[data[n][2]][3] = data[n][3]

                    # Gets header of previous set
                    h = data[data[n][0]][3]

                    # If the previous set is old
                    # makes a new set to store neighbour
                    if data[h][0] <= i:
                        head = data[h][1]
                        data[h][1] = c
                        data[head][3] = c
                        back = h
                        flag = i + 1
                        data.append([flag, head, -1, back])
                        h = c
                        c += 1

                    # Adds neighbour to new set
                    data[n][2] = data[h][2]
                    if data[h][2] != -1:
                        data[data[h][2]][3] = n

                    data[n][0] = h
                    data[n][3] = h
                    data[h][2] = n

    return ordering


def lex_depth_first_search(G, s):
    # Initialises empty labels
    labels = [[] for v in G.V]
    ordering = []

    # Populates source vertex's label
    labels[s] = [len(G.V)]

    # Loops through all vertices 
    for i in range(len(G.V)):
        # Chooses largest label
        v = len(G.V) - labels[::-1].index(max(labels)) - 1
        # Adds vertex to ordering
        ordering.append(v)

        if G.is_matrix == 0:
            for n in G.N[v]:
                if n not in ordering:
                    labels[n].insert(0, i + 1)
        else:
            for n in range(len(G.V)):
                if n not in ordering and G.N[v][n] == 1:
                    labels[n].insert(0, i + 1)

        labels[v] = []

    return ordering


def run_algorithms(filename, data_type):

    timings = []

    try:

        print("\nStarting Algorithms on " + filename + ":")
        if data_type == 0:
            print("\nGetting adjacency list data from file\n")
            g = Graph(filename, data_type)
        else:
            print("\nGetting adjacency matrix data from file")
            g = Graph(filename, data_type)
            
        print("Running DFS:")
        average = 0
        for i in range(5):
            start = time.perf_counter()
            return_val = preparation(g, 0)
            finish = time.perf_counter()
            average += (finish - start)
        print("Returned path:", return_val[0])
        # print("Ordering:", return_val[1])
        print(f"Time for completion: {average/5:0.6f} seconds\n")
        timings.append(average/5)

        print("Running BFS:")
        average = 0
        for i in range(5):
            start = time.perf_counter()
            return_val = breadth_first_search(g, 0)
            finish = time.perf_counter()
            average += (finish - start)
        print("Returned path:", return_val[0])
        # print("Ordering: ", return_val[1])
        print(f"Time for completion: {average/5:0.6f} seconds\n")
        timings.append(average/5)

        print("Running LexBFS:")
        average = 0
        for i in range(5):
            start = time.perf_counter()
            return_val = lex_breadth_first_search(g, 0)
            finish = time.perf_counter()
            average += (finish - start)
        print("Ordering:", return_val)
        print(f"Time for completion: {average/5:0.6f} seconds\n")
        timings.append(average/5)

        print("Running LexDFS:")
        average = 0
        for i in range(5):
            start = time.perf_counter()
            return_val = lex_depth_first_search(g, 0)
            finish = time.perf_counter()
            average += (finish - start)
        print("Ordering:", return_val)
        print(f"Time for completion: {average/5:0.6f} seconds\n")
        timings.append(average/5)

        return timings

    except FileNotFoundError:
        print("File could not be found")
        return -1


def run_lexbfs_algorithms(filename, data_type):

    timings = []

    try:
        print("\nStarting Algorithms on " + filename + ":")
        if data_type == 0:
            print("\nGetting adjacency list data from file\n")
            g = Graph(filename, data_type)
        else:
            print("\nGetting adjacency matrix data from file")
            g = Graph(filename, data_type)

        print("Running LexBFS:")
        average = 0
        for i in range(5):
            start = time.perf_counter()
            return_val = lex_breadth_first_search(g, 0)
            finish = time.perf_counter()
            average += (finish - start)
        print("Ordering:", return_val)
        print(f"Time for completion: {average/5:0.6f} seconds\n")
        timings.append(average/5)

        print("Running Set Partition LexBFS:")
        average = 0
        for i in range(5):
            start = time.perf_counter()
            return_val = set_partition_lex_bfs(g, 0)
            finish = time.perf_counter()
            average += (finish - start)
        print("Ordering:", return_val)
        print(f"Time for completion: {average / 5:0.6f} seconds\n")
        timings.append(average / 5)

    except FileNotFoundError:
        print("File could not be found")
        return -1

    return timings

if __name__ == "__main__":
    timing_data = []
    if len(sys.argv) >= 2:
        for i in range(1, len(sys.argv)):
            timing_data.append(run_algorithms(sys.argv[i]))

    else:
        print("Please use correct format when calling program: Algorithms.py <list_of_file>")
