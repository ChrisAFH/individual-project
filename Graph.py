import csv

class Graph:

    def get_neighbours(self):
        # Derives list of neighbouring vertices
        if self.is_matrix == 0:
            for pair in self.E:
                self.N[pair[0]].append(pair[1])
        # Derives matrix of neighbouring vertices 
        else:
            for pair in self.E:
                self.N[pair[0]][pair[1]] = 1


    def __init__(self, *args):
        """
        Initialisation of a graph with quoted vertices
        and edges.
        """

        if len(args) == 2:
            self.V = []
            self.E = []
            # Opens .csv file using filename given
            with open(args[0], newline=("")) as csvfile:
                # Gets all data from file
                data = csv.reader(csvfile, delimiter=',', quotechar='\"')
                rowdata = []
                for row in data:
                    rowdata.append(row)
                # Removes headers    
                rowdata.remove(rowdata[0])
                # Loops through data and adds vertex to set as well as adding edge to set after converting from
                # text to list
                for row in rowdata:
                    self.V.append(int(row[0]))
                    for n in row[1].split(" "):
                        n = n[1:-1].split(",")
                        self.E.append([int(n[0]),int(n[1])])
            
            # Checks if adjacency matrix or list
            if args[1] == 0:
                self.is_matrix = 0
                self.N = [[] for i in range(len(self.V))]
                self.get_neighbours()
            else:
                self.is_matrix = 1
                self.N = [[0 for i in range(len(self.V))] for j in range(len(self.V))]
                self.get_neighbours()
    
    # Returns vertex set
    def get_V(self):
        return self.V

    # # Returns edge set
    def get_E(self):
        return self.E

    # # Return adjacency list 
    def get_N(self):
        return self.N

