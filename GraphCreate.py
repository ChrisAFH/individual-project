import random
import sys
#
#
# # Writes graph data in accordance with input


def write_graph(n, a):
    try:
        # empty n x n matrix
        adj = [[0 for i in range(n)] for j in range(n)]

        for i in range(n-1):

            count = adj[i].count(1)
            choices = []
            for k in range(i+1, n):
                if not adj[k].count(1) == a:
                    choices.append(k)
            for j in range(a - count):

                index = random.choice(choices)
                adj[i][index] = 1
                adj[index][i] = 1
                choices.remove(index)
                count += 1

    except IndexError:
        return -1

    return adj


# Converts graph data to csv format for storage
def data_to_csv(data):
    text = "vertex,edges\n"
    for i in range(len(data[0])):
        text += str(i) + ",\""
        for j in range(len(data[0])):
            if data[i][j] == 1:
                text += "[" + str(i) + "," + str(j) + "] "

        text = text[:-1] + "\"\n"

    return text


if __name__ == '__main__':
    # # Gets information from the user
    # filename = input("What do you want to name the file:\n") + ".csv"
    # n = int(input("How many vertices do you want on the graph:\n"))
    # a = int(input("How many neighbours should each vertex have:\n"))

    if len(sys.argv) == 4:
        filename = sys.argv[1]
        n = int(sys.argv[2])
        a = int(sys.argv[3])

        # Validates that the inputs are allowed
        if a > n-1:
            print("vertex degrees exceeds the allowed limit")
        elif a <= 1:
            print("vertex degrees is below allowed limit")
        else:
            data = -1
            # # Creates graph data until

            while not isinstance(data, list):
                print("Collecting data...")
                data = write_graph(n, a)

            print("Data correct... writing to file")
            file = open(filename, 'w')
            file.write(data_to_csv(data))
            file.close()

    else:
        print("Incorrect usage of file:\n GraphCreate.py <filename> <number of vertices> <number of neighbours")

